#!/bin/bash
#mkdir p 01-hello
project_name="04-hello-node"
lib_name="hello"
cargo new --lib ${project_name} --name ${lib_name}
cd ${project_name}
cargo add wasm-bindgen
cd ..

#echo 'wasm-bindgen = "0.2.74"' >> ./${project_name}/Cargo.toml
echo "" >> ./${project_name}/Cargo.toml
echo "[lib]" >> ./${project_name}/Cargo.toml
echo 'name = "hello"' >> ./${project_name}/Cargo.toml
echo 'path = "src/lib.rs"' >> ./${project_name}/Cargo.toml
echo 'crate-type =["cdylib"]' >> ./${project_name}/Cargo.toml

cat > ./${project_name}/index.js <<- EOM
const pkg = require('./pkg/hello.js')
const {hello} = pkg

console.log(hello("Bob")) 
console.log(hello("Jane")) 
console.log(hello("John")) 
EOM

cat > ./${project_name}/build.sh <<- EOM
#!/bin/bash
wasm-pack build --release --target nodejs
EOM
chmod +x ./${project_name}/build.sh

cat > ./${project_name}/run.sh <<- EOM
#!/bin/bash
node index.js
EOM
chmod +x ./${project_name}/run.sh

cat > ./${project_name}/src/lib.rs <<- EOM
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn hello(s: String) -> String {
  let r = String::from("👋 hello ");
  
  return r + &s;
}
EOM