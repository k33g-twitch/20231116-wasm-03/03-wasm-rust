#!/bin/bash
#mkdir p 01-hello
project_name="01-hello"
lib_name="hello"
cargo new --lib ${project_name} --name ${lib_name}
cd ${project_name}
cargo add wasm-bindgen
cd ..

#echo 'wasm-bindgen = "0.2.74"' >> ./${project_name}/Cargo.toml
echo "" >> ./${project_name}/Cargo.toml
echo "[lib]" >> ./${project_name}/Cargo.toml
echo 'name = "hello"' >> ./${project_name}/Cargo.toml
echo 'path = "src/lib.rs"' >> ./${project_name}/Cargo.toml
echo 'crate-type =["cdylib"]' >> ./${project_name}/Cargo.toml

cat > ./${project_name}/index.html <<- EOM
<html>
	<head>
		<meta charset="utf-8"/>
	</head>
	<body>
		<h1>WASM Experiments</h1>
    <h2>Look at the console...</h2>

    <script type="module">
      import init, { hello } from './pkg/hello.js'

      async function run() {
        await init()
        console.log(hello("Bob")) 
        console.log(hello("Jane")) 
        console.log(hello("John")) 
      }
      run();
    </script>

	</body>
</html>
EOM

cp ./favicon.ico ./${project_name}

cat > ./${project_name}/index.js <<- EOM
const fastify = require('fastify')({ logger: true })
const path = require('path')

// Serve the static assets
fastify.register(require('fastify-static'), {
  root: path.join(__dirname, ''),
  prefix: '/'
})

const start = async () => {
  try {
    await fastify.listen(8080, "0.0.0.0")
    console.log("server listening on:", fastify.server.address().port)

  } catch (error) {
    fastify.log.error(error)
  }
}
start()
EOM

cat > ./${project_name}/build.sh <<- EOM
#!/bin/bash
wasm-pack build --release --target web
EOM
chmod +x ./${project_name}/build.sh

cat > ./${project_name}/serve.sh <<- EOM
#!/bin/bash
node index.js
EOM
chmod +x ./${project_name}/serve.sh

cat > ./${project_name}/src/lib.rs <<- EOM
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn hello(name: String) -> String {
  let message = String::from("👋 hello ");
  
  return message + &name;
}
EOM