#!/bin/bash
#mkdir p 01-hello
project_name="03-hello-dom"
lib_name="hello"
cargo new --lib ${project_name} --name ${lib_name}
cd ${project_name}
cargo add serde --features "derive"
cargo add serde-wasm-bindgen
cargo add wasm-bindgen
cargo add web-sys --features "Document Element HtmlElement Node Window"
cd ..

#echo 'wasm-bindgen = "0.2.74"' >> ./${project_name}/Cargo.toml
echo "" >> ./${project_name}/Cargo.toml
echo "[lib]" >> ./${project_name}/Cargo.toml
echo 'name = "hello"' >> ./${project_name}/Cargo.toml
echo 'path = "src/lib.rs"' >> ./${project_name}/Cargo.toml
echo 'crate-type =["cdylib"]' >> ./${project_name}/Cargo.toml

cat > ./${project_name}/index.html <<- EOM
<html>
  <head>
    <meta charset="utf-8"/>
  </head>
  <body>
    <h1>WASM Experiments</h1>

    <script type="module">
      import init from './pkg/hello.js'

      async function run() {
        await init()
      }
      run();
    </script>

  </body>
</html>

EOM

cp ./favicon.ico ./${project_name}

cat > ./${project_name}/index.js <<- EOM
const fastify = require('fastify')({ logger: true })
const path = require('path')

// Serve the static assets
fastify.register(require('fastify-static'), {
  root: path.join(__dirname, ''),
  prefix: '/'
})

const start = async () => {
  try {
    await fastify.listen(8080, "0.0.0.0")
    console.log("server listening on:", fastify.server.address().port)

  } catch (error) {
    fastify.log.error(error)
  }
}
start()
EOM

cat > ./${project_name}/build.sh <<- EOM
#!/bin/bash
wasm-pack build --release --target web
EOM
chmod +x ./${project_name}/build.sh

cat > ./${project_name}/serve.sh <<- EOM
#!/bin/bash
node index.js
EOM
chmod +x ./${project_name}/serve.sh

cat > ./${project_name}/src/lib.rs <<- EOM
use wasm_bindgen::prelude::*;

// Called by the JS entry point to run the example
#[wasm_bindgen(start)]
pub fn run() -> Result<(), JsValue> {
    // Use web_sys's global window function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global window exists");
    let document = window.document().expect("should have a document on window");
    let body = document.body().expect("document should have a body");

    // Manufacture the element we're gonna append
    let h1 = document.create_element("h1")?;
    h1.set_text_content(Some("👋 Hello from Rust! 🦀"));

    let h2 = document.create_element("h2")?;
    h2.set_text_content(Some("😍 I'm a subtitle"));

    body.append_child(&h1)?;
    body.append_child(&h2)?;

    Ok(())
}

EOM