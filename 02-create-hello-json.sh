#!/bin/bash
#mkdir p 01-hello
project_name="02-hello-json"
lib_name="hello"
cargo new --lib ${project_name} --name ${lib_name}
cd ${project_name}
cargo add serde --features "derive"
cargo add serde-wasm-bindgen
cargo add wasm-bindgen
cd ..

#echo 'wasm-bindgen = "0.2.74"' >> ./${project_name}/Cargo.toml
echo "" >> ./${project_name}/Cargo.toml
echo "[lib]" >> ./${project_name}/Cargo.toml
echo 'name = "hello"' >> ./${project_name}/Cargo.toml
echo 'path = "src/lib.rs"' >> ./${project_name}/Cargo.toml
echo 'crate-type =["cdylib"]' >> ./${project_name}/Cargo.toml

cat > ./${project_name}/index.html <<- EOM
<html>
	<head>
		<meta charset="utf-8"/>
	</head>
	<body>
		<h1>WASM Experiments</h1>
    <h2>Look at the console...</h2>

    <script type="module">
      import init, { send } from './pkg/hello.js'

      async function run() {
        await init()
        let message = {text:"Salut", author: "Bob Morane"}

        let response = send(message)

        console.log(response)

      }
      run();
    </script>

	</body>
</html>
EOM

cp ./favicon.ico ./${project_name}

cat > ./${project_name}/index.js <<- EOM
const fastify = require('fastify')({ logger: true })
const path = require('path')

// Serve the static assets
fastify.register(require('fastify-static'), {
  root: path.join(__dirname, ''),
  prefix: '/'
})

const start = async () => {
  try {
    await fastify.listen(8080, "0.0.0.0")
    console.log("server listening on:", fastify.server.address().port)

  } catch (error) {
    fastify.log.error(error)
  }
}
start()
EOM

cat > ./${project_name}/build.sh <<- EOM
#!/bin/bash
wasm-pack build --release --target web
EOM
chmod +x ./${project_name}/build.sh

cat > ./${project_name}/serve.sh <<- EOM
#!/bin/bash
node index.js
EOM
chmod +x ./${project_name}/serve.sh

cat > ./${project_name}/src/lib.rs <<- EOM
use wasm_bindgen::prelude::*;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct Message {
    pub text: String,
    pub author: String,
}

#[derive(Serialize, Deserialize)]
pub struct Response {
    pub text: String,
    pub author: String,
    pub message_text: String,
}

#[wasm_bindgen]
pub fn send(value: JsValue) -> Result<JsValue, JsValue> {
    // deserialize value (parameter) to message
    let message: Message = serde_wasm_bindgen::from_value(value)?;

    let response = Response {
        text: String::from(format!("👋 hello {}", message.author)),
        author: String::from("🦀"),
        message_text: String::from(format!("📝 your message:{}", message.text)),
    };

    // serialize response to JsValue
    return Ok(serde_wasm_bindgen::to_value(&response)?)
}
EOM